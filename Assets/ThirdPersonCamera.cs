using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using ExitGames.Client.Photon;
using Photon.Realtime;

public class ThirdPersonCamera : MonoBehaviour
{
    private PhotonView photonView;
    public Transform target;
    public float x = 180;
    public float y = 15;
    public float xSpeed = 3000;
    public float ySpeed = 1000;
    public float distance  = 2;
    public float disSpeed = 200;

    public float minDistance = 1;
    public float maxDistance = 3;

    private Quaternion rotationEular;
    private Vector3 cameraPosition;
    void Start()
    {
        //ani.enabled = false;
        photonView = gameObject.GetComponent<PhotonView>();
    }
    private void FixedUpdate()
    {
        if (photonView.IsMine)
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                x += Input.GetAxis("Mouse X") * xSpeed * Time.deltaTime;
                y += Input.GetAxis("Mouse Y") * ySpeed * Time.deltaTime;
                y = Mathf.Clamp(y, -15, 15);
            }

            if (x > 360)
                x -= 360;
            else if (x < 0)
                x += 360;

            distance -= Input.GetAxis("Mouse ScrollWheel") * disSpeed * Time.deltaTime;
            distance = Mathf.Clamp(distance, minDistance, maxDistance);

            rotationEular = Quaternion.Euler(y, x, 0);
            cameraPosition = rotationEular * new Vector3(0, 0, -distance) + target.position;

            Camera.main.transform.rotation = rotationEular;
            Camera.main.transform.position = cameraPosition;
        }
    }
}
