using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using ExitGames.Client.Photon;
using Photon.Realtime;
public class PlayerController : MonoBehaviour
{
    private PhotonView photonView;
    public Animator ani;
    // Start is called before the first frame update
    void Start()
    {
        //ani.enabled = false;
        photonView = gameObject.GetComponent<PhotonView>();
    }
    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                ani.SetBool("sw_run", true);
            }
            else if (Input.GetKeyUp(KeyCode.W))

            {
                ani.SetBool("sw_run", false);
            }

            if (Input.GetKey(KeyCode.W))
                transform.Translate(0, 0, 3 * Time.deltaTime);
            else
                transform.Translate(Vector3.zero);

            if (Input.GetKey(KeyCode.D))
                transform.Rotate(0, 100 * Time.deltaTime, 0, Space.Self);
            else if (Input.GetKey(KeyCode.A))
                transform.Rotate(0, -100 * Time.deltaTime, 0, Space.Self);
            else
                transform.Translate(Vector3.zero);

            if (Input.GetKeyDown(KeyCode.Space))
                photonView.RPC("ChangeFloorColor", RpcTarget.All);
        }
    }


    [PunRPC]
    private void ChangeFloorColor()
    {
        GameObject.Find("Plane").GetComponent<Renderer>().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
    }
}
